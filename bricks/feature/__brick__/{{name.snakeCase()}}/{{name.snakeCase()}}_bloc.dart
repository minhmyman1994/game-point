import 'package:flutter_bloc/flutter_bloc.dart';
import './{{name.snakeCase()}}_event.dart';
class {{name.pascalCase()}}Bloc extends Bloc <{{name.pascalCase()}}Event, int>{
{{name.pascalCase()}}Bloc(super.initialState);
}