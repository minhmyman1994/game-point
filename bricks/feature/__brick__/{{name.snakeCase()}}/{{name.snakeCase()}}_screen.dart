import 'package:flutter/material.dart';
import 'package:count_point_game/shared_widgets/base_scaffold.dart';

class {{name.pascalCase()}}Screen extends StatelessWidget {
  const {{name.pascalCase()}}Screen ({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
    title: '{{name.pascalCase()}}',
    body: Text('{{name.pascalCase()}}'));
  }
}
