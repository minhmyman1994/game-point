import 'package:count_point_game/feature/game_random_color/game_random_color_bloc.dart';
import 'package:count_point_game/feature/game_random_color/random_color_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BaseScaffold extends StatelessWidget {
  const BaseScaffold({Key? key, required this.body, this.title})
      : super(key: key);
  final Widget body;
  final String? title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title ?? ''),
      ),
      body: SafeArea(
        child: body,
      ),
    );
  }
}
