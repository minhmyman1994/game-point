import 'package:count_point_game/common/constants.dart';
import 'package:flutter/material.dart';

typedef ButtonClick = Function();

class ButtonCustom extends StatelessWidget {
  const ButtonCustom({
    Key? key,
    required this.title,
    this.backgroundColor,
    this.titleStyle,
    this.onPress,
  }) : super(key: key);
  final String title;
  final Color? backgroundColor;
  final TextStyle? titleStyle;
  final ButtonClick? onPress;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
        color: backgroundColor ?? Theme.of(context).primaryColor,
        padding: PAD_H16_V12,
        child: Text(
          title,
          style: titleStyle ??
              const TextStyle(
                fontSize: 14,
                color: Colors.white,
              ),
        ),
      ),
    );
  }
}
