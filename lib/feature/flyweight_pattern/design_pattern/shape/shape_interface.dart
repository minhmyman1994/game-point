import 'package:flutter/material.dart';

abstract class ShapeInterface {
  Widget render(double x, double y);
}
