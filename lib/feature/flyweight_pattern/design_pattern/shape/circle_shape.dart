import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/shape_interface.dart';
import 'package:flutter/material.dart';

class CircleShape implements ShapeInterface {
  final double diameter;
  final Color color;

  CircleShape({required this.diameter, required this.color});

  @override
  Widget render(double x, double y) {
    return Positioned(
        left: x,
        bottom: y,
        child: Container(
          width: diameter,
          height: diameter,
          decoration: BoxDecoration(
            color: color,
            shape: BoxShape.circle,
          ),
        ));
  }
}
