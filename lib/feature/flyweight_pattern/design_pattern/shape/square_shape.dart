import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/shape_interface.dart';
import 'package:flutter/material.dart';

class SquareShape implements ShapeInterface {
  final double width;
  final double height;
  final Color color;

  SquareShape({
    required this.width,
    required this.height,
    required this.color,
  });

  @override
  Widget render(double x, double y) {
    return Positioned(
      left: x,
      bottom: y,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: color,
        ),
      ),
    );
  }
}
