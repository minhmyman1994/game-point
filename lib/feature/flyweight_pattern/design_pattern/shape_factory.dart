import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/circle_shape.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/shape_interface.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/square_shape.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shapetype_enum.dart';
import 'package:flutter/material.dart';

class ShapeFactory {
  ShapeInterface createShape(ShapeType shapeType) {
    switch (shapeType) {
      case ShapeType.circle:
        return CircleShape(diameter: 10, color: Colors.red);
      case ShapeType.square:
        return SquareShape(width: 10, height: 15, color: Colors.blue);
      default:
        throw Exception("Shape type '$shapeType' is not Supported");
    }
  }
}
