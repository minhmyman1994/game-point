import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/shape_interface.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape_factory.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shapetype_enum.dart';
import 'package:flutter/material.dart';

class ShapeFlyWeightFactory {
  final ShapeFactory shapeFactory;
  final Map<ShapeType, ShapeInterface> shapeMaps =
      <ShapeType, ShapeInterface>{};

  ShapeFlyWeightFactory({required this.shapeFactory});
  ShapeInterface getShape(ShapeType shapeType) {
    if (!shapeMaps.containsKey(shapeType)) {
      try {
        final shape = shapeFactory.createShape(shapeType);
        shapeMaps[shapeType] = shape;
      } catch (e) {
        debugPrint(e.toString());
      }
    }
    if (shapeMaps[shapeType] == null) {
      throw Exception('ShapeMaps is not contain $shapeType');
    } else {
      return shapeMaps[shapeType]!;
    }
  }

  int getShapeInstanceCount() {
    return shapeMaps.length;
  }
}
