import 'dart:math';

import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/shape_interface.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape_factory.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape_flyweight_factory.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shapetype_enum.dart';
import 'package:count_point_game/feature/flyweight_pattern/shape_drawer/shape_drawer.dart';
import 'package:flutter/material.dart';

const SHAPES_COUNT = 100;

class FlyweightPatternScreen extends StatefulWidget {
  const FlyweightPatternScreen({Key? key}) : super(key: key);

  @override
  State<FlyweightPatternScreen> createState() => _FlyweightPatternScreenState();
}

class _FlyweightPatternScreenState extends State<FlyweightPatternScreen> {
  final ShapeFactory _shapeFactory = ShapeFactory();

  late final ShapeFlyWeightFactory _shapeFlyWeightFactory;
  int _shapeInstanceCount = 0;
  bool _useFlyWeightPattern = false;
  List<ShapeInterface> shapes = <ShapeInterface>[];
  @override
  void initState() {
    _shapeFlyWeightFactory = ShapeFlyWeightFactory(shapeFactory: _shapeFactory);
    _buildShapes();

    super.initState();
  }

  ShapeType _getRandomShapeType() {
    const values = ShapeType.values;
    return values[Random().nextInt(values.length)];
  }

  void _buildShapes() {
    int shapeInstanceCount = 0;
    shapes = <ShapeInterface>[];
    for (int i = 0; i < SHAPES_COUNT; i++) {
      final shapeType = _getRandomShapeType();
      try {
        final shapeInterface = _useFlyWeightPattern
            ? _shapeFlyWeightFactory.getShape(shapeType)
            : _shapeFactory.createShape(shapeType);
        shapeInstanceCount++;
        shapes.add(shapeInterface);
      } catch (e) {
        debugPrint('Exception type : ${e.runtimeType}');
      }
    }
    print('Minh ne');

    setState(() {
      _shapeInstanceCount = _useFlyWeightPattern
          ? _shapeFlyWeightFactory.getShapeInstanceCount()
          : shapeInstanceCount;
    });
  }

  void _toggleUseFlyWeightFactory(bool value) {
    setState(() {
      _useFlyWeightPattern = value;
    });
    _buildShapes();
  }

  void _testException() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flyweight Pattern Screen'),
      ),
      body: Column(
        children: [
          const Text('Use flyweight factory'),
          Text('Count of Shapes: $_shapeInstanceCount'),
          Switch.adaptive(
              value: _useFlyWeightPattern,
              onChanged: _toggleUseFlyWeightFactory),
          InkWell(
            onTap: _testException,
            child: Container(
              color: Colors.blue,
              padding: const EdgeInsets.all(20),
              child: const Text('Test Exception'),
            ),
          ),
          Expanded(
            child: Stack(
              children: List.generate(
                  shapes.length, (index) => ShapeDrawer(shape: shapes[index])),
            ),
          ),
        ],
      ),
    );
  }
}
