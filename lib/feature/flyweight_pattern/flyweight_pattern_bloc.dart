import 'package:flutter_bloc/flutter_bloc.dart';
import 'flyweight_pattern_event.dart';

class FlyweightPatternBloc extends Bloc<FlyweightPatternEvent, int> {
  FlyweightPatternBloc(super.initialState);
}
