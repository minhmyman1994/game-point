import 'dart:math';

import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/shape_interface.dart';
import 'package:flutter/material.dart';

class ShapeDrawer extends StatefulWidget {
  const ShapeDrawer({Key? key, required this.shape}) : super(key: key);
  final ShapeInterface shape;

  @override
  State<ShapeDrawer> createState() => _ShapeDrawerState();
}

class _ShapeDrawerState extends State<ShapeDrawer>
    with SingleTickerProviderStateMixin {
  // late final AnimationController controller;
  late final Animation<double> fireOfRed;

  double get getRandom => Random().nextInt(300).toDouble();
  @override
  void initState() {
    // controller = AnimationController(vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    // controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.shape.render(getRandom, getRandom);
  }
}
