import 'package:flutter/material.dart';
import 'package:count_point_game/shared_widgets/base_scaffold.dart';

class GamePointScreen extends StatelessWidget {
  const GamePointScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const BaseScaffold(
      title: 'GamePoint',
      body: Center(
        child: Text('Coming soon'),
      ),
    );
  }
}
