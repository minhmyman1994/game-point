import 'dart:math';

import 'package:count_point_game/common/constants.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/circle_shape.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/shape_interface.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/square_shape.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape_factory.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape_flyweight_factory.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shapetype_enum.dart';
import 'package:count_point_game/feature/flyweight_pattern/shape_drawer/shape_drawer.dart';
import 'package:count_point_game/feature/game_random_color/game_random_color_bloc.dart';
import 'package:count_point_game/feature/game_random_color/game_random_color_event.dart';
import 'package:count_point_game/feature/game_random_color/random_color_state.dart';
import 'package:count_point_game/shared_widgets/base_scaffold.dart';
import 'package:count_point_game/shared_widgets/button_custom.dart';
import 'package:flutter/material.dart';
import 'package:collection/collection.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

const SHAPES_COUNT = 100;

class GameRandomColorScreen extends StatefulWidget {
  const GameRandomColorScreen({Key? key}) : super(key: key);

  @override
  State<GameRandomColorScreen> createState() => _GameRandomColorScreenState();
}

class _GameRandomColorScreenState extends State<GameRandomColorScreen> {
  final ShapeFactory _shapeFactory = ShapeFactory();

  late final ShapeFlyWeightFactory _shapeFlyWeightFactory;
  int _shapeInstanceCount = 0;
  bool _useFlyWeightPattern = false;
  List<ShapeInterface> shapes = <ShapeInterface>[];
  @override
  void initState() {
    _shapeFlyWeightFactory = ShapeFlyWeightFactory(shapeFactory: _shapeFactory);
    _buildShapes();
    super.initState();
  }

  ShapeType _getRandomShapeType() {
    const values = ShapeType.values;
    return values[Random().nextInt(values.length)];
  }

  void _buildShapes() {
    int shapeInstanceCount = 0;
    shapes = <ShapeInterface>[];
    for (int i = 0; i < SHAPES_COUNT; i++) {
      final shapeType = _getRandomShapeType();
      try {
        final shapeInterface = _shapeFlyWeightFactory.getShape(shapeType);
        shapeInstanceCount++;
        shapes.add(shapeInterface);
      } catch (e) {
        debugPrint('Exception type : ${e.runtimeType}');
      }
    }
    debugPrint('Minh ne');

    setState(() {
      _useFlyWeightPattern = true;
      _shapeInstanceCount = shapeInstanceCount;
    });
  }

  Future<void> _handleRefresh() async {
    final randomColorBloc = BlocProvider.of<GameRandomColorBloc>(context);
    randomColorBloc.add(RefreshRandomColorStart());
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      title: 'Game Random Color',
      body: BlocBuilder<GameRandomColorBloc, RandomColorState>(
          builder: (_, state) {
        final shapesState = state.shapes;
        final sumOfSquare = state.sumOfSquare;
        final sumOfCircle = state.sumOfCircle;
        return Column(
          children: [
            Text('Count of Shapes: $_shapeInstanceCount'),
            Padding(
              padding: PAD_H16_V12,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Count of Circle: $sumOfCircle'),
                  Text('Count of Square: $sumOfSquare'),
                ],
              ),
            ),
            ButtonCustom(
              title: 'Refresh',
              onPress: _handleRefresh,
            ),
            Expanded(
              child: Stack(
                children: List.generate(shapesState.length,
                    (index) => ShapeDrawer(shape: shapesState[index])),
              ),
            )
          ],
        );
      }),
    );
  }
}
