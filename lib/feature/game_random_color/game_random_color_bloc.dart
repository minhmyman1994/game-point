import 'dart:math';

import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/circle_shape.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/shape_interface.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/square_shape.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape_factory.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape_flyweight_factory.dart';
import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shapetype_enum.dart';
import 'package:count_point_game/feature/flyweight_pattern/flyweight_pattern_screen.dart';
import 'package:count_point_game/feature/game_random_color/random_color_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import './game_random_color_event.dart';

class GameRandomColorBloc extends Bloc<GameRandomColorEvent, RandomColorState> {
  GameRandomColorBloc(super.initialState) {
    on<RefreshRandomColorStart>(onRefreshRandomColorStart);
    on<RefreshRandomColorProgressing>(onRefreshRandomColorProgressing);
    on<RefreshRandomColorDone>(onRefreshRandomColorDone);
  }

  final ShapeFlyWeightFactory _shapeFlyWeightFactory =
      ShapeFlyWeightFactory(shapeFactory: ShapeFactory());
  void onRefreshRandomColorStart(GameRandomColorEvent event, Emitter emit) {
    add(RefreshRandomColorProgressing());
  }

  void onRefreshRandomColorProgressing(
      GameRandomColorEvent event, Emitter emit) {
    handleWhenProgressing();
  }

  void onRefreshRandomColorDone(GameRandomColorEvent event, Emitter emit) {
    final shapes = getShapes();
    emit(RandomColorState(
        shapes: shapes,
        sumOfSquare: _getCountShapeSpecific(ShapeType.square),
        sumOfCircle: _getCountShapeSpecific(ShapeType.circle)));
  }

  Future<void> handleWhenProgressing() async {
    EasyLoading.showProgress(0.5);
    await Future.delayed(const Duration(milliseconds: 3500));
    EasyLoading.dismiss();

    add(RefreshRandomColorDone());
  }

  List<ShapeInterface> getShapes() {
    final shapes = <ShapeInterface>[];
    for (int i = 0; i < SHAPES_COUNT; i++) {
      final shapeType = _getRandomShapeType();
      try {
        final shapeInterface = _shapeFlyWeightFactory.getShape(shapeType);
        shapes.add(shapeInterface);
      } catch (e) {
        debugPrint('Exception type : ${e.runtimeType}');
      }
    }
    return shapes;
  }

  ShapeType _getRandomShapeType() {
    const values = ShapeType.values;
    return values[Random().nextInt(values.length)];
  }

  int _getCountShapeSpecific(ShapeType shapeType) {
    debugPrint('Hello em');

    switch (shapeType) {
      case ShapeType.square:
        {
          return state.shapes
              .where((element) => element.runtimeType == SquareShape)
              .toList()
              .length;
        }
      case ShapeType.circle:
        {
          return state.shapes
              .where((element) => element.runtimeType == CircleShape)
              .toList()
              .length;
        }
      default:
        throw ("An error occured");
    }
  }
}
