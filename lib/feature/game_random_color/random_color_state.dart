import 'package:count_point_game/feature/flyweight_pattern/design_pattern/shape/shape_interface.dart';

class RandomColorState {
  List<ShapeInterface> shapes = [];
  int sumOfSquare = 0;
  int sumOfCircle = 0;

  RandomColorState({
    required this.shapes,
    required this.sumOfSquare,
    required this.sumOfCircle,
  });
}
