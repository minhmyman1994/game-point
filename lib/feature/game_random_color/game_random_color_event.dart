import 'package:count_point_game/feature/game_random_color/game_random_color_screen.dart';
import 'package:flutter/src/foundation/key.dart';

abstract class GameRandomColorEvent {}

class RefreshRandomColorStart extends GameRandomColorEvent {}

class RefreshRandomColorProgressing extends GameRandomColorEvent {}

class RefreshRandomColorDone extends GameRandomColorEvent {}
