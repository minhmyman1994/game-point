import 'package:count_point_game/routes/route_path.dart';
import 'package:count_point_game/shared_widgets/button_custom.dart';
import 'package:flutter/material.dart';
import 'package:count_point_game/shared_widgets/base_scaffold.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);
  void _navigateToGamePoint(BuildContext context) {
    Navigator.of(context).pushNamed(RoutePath.gamePointScreen);
  }

  void _navigateToGameRandomColor(BuildContext context) {
    Navigator.of(context).pushNamed(RoutePath.gameRandomColorScreen);
  }

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
        title: 'Home',
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ButtonCustom(
                title: 'Navigate To Game Point',
                onPress: () => _navigateToGamePoint(context),
              ),
              const SizedBox(
                height: 20,
              ),
              ButtonCustom(
                title: 'Navigate To Game RanDom Color',
                onPress: () => _navigateToGameRandomColor(context),
              ),
            ],
          ),
        ));
  }
}
