import 'package:flutter/material.dart';
import 'package:count_point_game/shared_widgets/base_scaffold.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(title: 'Profile', body: Text('Profile'));
  }
}
