class RoutePath {
  static const String initialRoute = '/';
  static const String homeScreen = '/homeScreen';
  static const String gamePointScreen = '/gamePointScreen';
  static const String profileScreen = '/profileScreen';
  static const String scoreScreen = '/scoreScreen';
  static const String gameRandomColorScreen = '/gameRandomColorScreen';
}
