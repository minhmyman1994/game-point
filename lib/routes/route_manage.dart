import 'package:count_point_game/feature/game_point/game_point_screen.dart';
import 'package:count_point_game/feature/game_random_color/game_random_color_bloc.dart';
import 'package:count_point_game/feature/game_random_color/game_random_color_screen.dart';
import 'package:count_point_game/feature/game_random_color/random_color_state.dart';
import 'package:count_point_game/feature/profile/profile_screen.dart';
import 'package:count_point_game/routes/route_path.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../feature/home/home_screen.dart';

class RouteManage {
  static Route<dynamic> generateRoutes(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case RoutePath.initialRoute:
        return MaterialPageRoute(builder: (context) => const HomeScreen());
      case RoutePath.homeScreen:
        return MaterialPageRoute(builder: (context) => const HomeScreen());

      case RoutePath.gamePointScreen:
        return MaterialPageRoute(builder: (context) => const GamePointScreen());
      case RoutePath.profileScreen:
        return MaterialPageRoute(builder: (context) => const ProfileScreen());
      case RoutePath.gameRandomColorScreen:
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                create: (BuildContext childContext) =>
                    GameRandomColorBloc(RandomColorState(
                      shapes: [],
                      sumOfCircle: 0,
                      sumOfSquare: 0,
                    )),
                child: const GameRandomColorScreen()));

      default:
        return MaterialPageRoute(builder: (context) => const HomeScreen());
    }
  }
}
